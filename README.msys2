This document contains instructions to build and install Wireshark on Windows
using GCC and MinGW-w64 with the MSYS2 distribution.

How to setup MSYS2:

1. Download and install MSYS2 from https://www.msys2.org.

2. Open the MSYS shell.

3. Update with "pacman -Syu" as many times as necessary. You also need to
   have base-devel installed:
   
    $ pacman -S base-devel.

How to build the Wireshark MSYS2 package:

1. Open the UCRT64 shell.

2. (Optional) Build and install some missing dependencies in the MSYS2 repository.

    $ cd $srcdir/packaging/msys2/mingw-w64-bcg729
    $ makepkg-mingw -si

    $ cd $srcdir/packaging/msys2/mingw-w64-libsmi
    $ makepkg-mingw -si

    $ cd $srcdir/packaging/msys2/mingw-w64-sbc
    $ makepkg-mingw -si

3. Build and install Wireshark

    $ cd $srcdir/packaging/msys2/mingw-w64-wireshark-git
    $ makepkg-mingw -si

The application should be launched using the same shell (UCRT64 in this case).

Currently the MSYS2 build has the following limitations compared to
the build using Visual Studio (MSVC):

* The Event Tracing for Windows (ETW) extcap cannot be compiled using MinGW-w64.

* Lua version is 5.1 (MSVC uses Lua 5.2) and does not have UTF-8 patches[1]. 

* Kerberos dependency for enhanced Kerberos dissection is not available
  in the repositories.

* AirPcap is not available and there is no plan to support it.

* There is currently no way to build a stand-alone distributable binary
  installer, similar to the Wireshark NSIS installer using MSVC.

References:

[1]https://github.com/Lekensteyn/lua-unicode
